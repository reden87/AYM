//
//  WeatherScreen.swift
//  AYM-assessment
//
//  Created by Gergo Nemeth on 2018. 08. 11..
//  Copyright © 2018. Gergo Nemeth. All rights reserved.
//

import UIKit
import Foundation

final class WeatherScreen: StaticBaseScreenController {
    // MARK: - Properties
    private let dataManager = WeatherDataManager()

    // MARK: - Components
    private let currentWeatherComponent = CurrentWeatherComponent.instantiateFromNib()
    private let weatherForecastComponent = WeatherForecastComponent.instantiateFromNib()

    // MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupComponents()
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        setupStatusBar()
        startListeningLocation()
    }
}

// MARK: - Setup
extension WeatherScreen {
    private func setupStatusBar() {
        UIApplication.shared.statusBarStyle = .default
    }

    private func setupComponents() {
        addComponent(currentWeatherComponent)
        addComponent(weatherForecastComponent)

        // temporary solution.. should find out why the content appears below the tab bar
        if let tabBarHeight = tabBarController?.tabBar.bounds.height {
            addEmptyComponent(with: tabBarHeight)
        }
    }

    private func startListeningLocation() {
        LocationManager.shared.getLocation { [weak self] response in
            switch response {
            case .location(let coordinates):
                self?.loadDataFor(latitude: coordinates.latitude, longitude: coordinates.longitude)

            case .error(let error):
                // TODO: display error in both components
                print("error occured: \(error)")
            }
        }
    }

    private func setupCurrentWeatherComponent(with viewModel: CurrentWeatherComponent.ViewModel) {
        currentWeatherComponent.setup(with: viewModel)
    }

    private func setupForecastComponent(with viewModel: WeatherForecastComponent.ViewModel) {
        weatherForecastComponent.setup(with: viewModel)
    }
}

// MARK: - Load Data
extension WeatherScreen {
    private func loadDataFor(latitude: Double, longitude: Double) {
        // current weather
        dataManager.getCurrentWeather(latitude: latitude, longitude: longitude) { [weak self] result in
            switch result {
            case .success(let viewModel):
                self?.setupCurrentWeatherComponent(with: viewModel)

            case .error(let error):
                // TODO: display error in current weather component only
                print("error occured: \(error)")
            }
        }

        // weather forecast
        dataManager.getForecast(latitude: latitude, longitude: longitude) { [weak self] result in
            switch result {
            case .success(let viewModel):
                self?.setupForecastComponent(with: viewModel)

            case .error(let error):
                // TODO: display error in weather forecast component only
                print("error occured: \(error)")
            }
        }
    }
}
