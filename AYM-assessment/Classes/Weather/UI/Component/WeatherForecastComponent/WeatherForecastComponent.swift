//
//  WeatherForecastComponent.swift
//  AYM-assessment
//
//  Created by Gergo Nemeth on 2018. 08. 11..
//  Copyright © 2018. Gergo Nemeth. All rights reserved.
//

import UIKit

final class WeatherForecastComponent: AYMComponent {
    // MARK: - Constants
    private enum Constants {
        static let animationTimeInterval: TimeInterval = 0.3
    }

    // MARK: - ViewModel
    struct ViewModel {
        let forecastItems: [WeatherForecastItem.ViewModel]
    }

    // MARK: - Outlets
    @IBOutlet private weak var activityIndicatorView: UIActivityIndicatorView!
    @IBOutlet private weak var stackView: UIStackView!

    // MARK: - Lifecycle
    override func awakeFromNib() {
        super.awakeFromNib()
        setupInitialState()
    }
}

// MARK: - Setup
extension WeatherForecastComponent {
    private func setupInitialState() {
        stackView.alpha = 0.0
    }

    func setup(with viewModel: ViewModel) {
        stackView.removeAllArrangedSubviews()

        for forecastItemViewModel in viewModel.forecastItems {
            let forecastView = WeatherForecastItem.instantiateFromNib()
            forecastView.setup(with: forecastItemViewModel)
            stackView.addArrangedSubview(forecastView)
        }

        UIView.animate(withDuration: Constants.animationTimeInterval, animations: { [weak self] in
            self?.activityIndicatorView.alpha = 0.0
            self?.stackView.alpha = 1.0
        }, completion: { [weak self] _ in
            self?.activityIndicatorView?.isHidden = true
        })
    }
}
