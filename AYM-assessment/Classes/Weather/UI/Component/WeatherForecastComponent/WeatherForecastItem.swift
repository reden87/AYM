//
//  WeatherForecastItem.swift
//  AYM-assessment
//
//  Created by Gergo Nemeth on 2018. 08. 11..
//  Copyright © 2018. Gergo Nemeth. All rights reserved.
//

import UIKit
import Kingfisher

final class WeatherForecastItem: UIView {
    // MARK: - Constants
    private enum Constants {
        static let celsiusSign = "°"
        static let imageLinkPattern = "https://openweathermap.org/img/w/%@.png"
    }

    // MARK: - ViewModel
    struct ViewModel {
        let date: Date
        let weatherIconName: String
        let maxTemperature: Double
        let minTemperature: Double
    }

    // MARK: - Outlets
    @IBOutlet private weak var weekLabel: UILabel!
    @IBOutlet private weak var weatherImageView: UIImageView!
    @IBOutlet private weak var maxTemperatureLabel: UILabel!
    @IBOutlet private weak var minTemperatureLabel: UILabel!

    // MARK: - Lifecycle
    override func awakeFromNib() {
        super.awakeFromNib()
        setupInitialState()
    }
}

// MARK: - Setup
extension WeatherForecastItem {
    private func setupInitialState() {
        weekLabel.text = ""
        weekLabel.textColor = AYMColor.black

        weatherImageView.image = nil

        maxTemperatureLabel.text = ""
        maxTemperatureLabel.textColor = AYMColor.black

        minTemperatureLabel.text = ""
        minTemperatureLabel.textColor = AYMColor.lightGrey
    }

    func setup(with viewModel: ViewModel) {
        weekLabel.text = viewModel.date.asString(format: .shortNameForDay).uppercased()

        let imageUrl = URL(string: String(format: Constants.imageLinkPattern, viewModel.weatherIconName))
        weatherImageView.kf.setImage(with: imageUrl)

        maxTemperatureLabel.text = String(describing: Int(viewModel.maxTemperature)) + Constants.celsiusSign
        minTemperatureLabel.text = String(describing: Int(viewModel.minTemperature)) + Constants.celsiusSign
    }
}
