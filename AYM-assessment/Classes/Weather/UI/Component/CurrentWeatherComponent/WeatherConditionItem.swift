//
//  WeatherConditionItem.swift
//  AYM-assessment
//
//  Created by Gergo Nemeth on 2018. 08. 11..
//  Copyright © 2018. Gergo Nemeth. All rights reserved.
//

import UIKit

final class WeatherConditionItem: UIView {
    // MARK: - ViewModel
    struct ViewModel {
        let conditionName: String
        let conditionValue: String
        let conditionUnit: String
    }

    // MARK: - Outlets
    @IBOutlet private weak var conditionValueLabel: UILabel!
    @IBOutlet private weak var conditionUnitLabel: UILabel!
    @IBOutlet private weak var conditionNameLabel: UILabel!

    // MARK: - Lifecycle
    override func awakeFromNib() {
        super.awakeFromNib()
        setupInitialState()
    }
}

// MARK: - Setup
extension WeatherConditionItem {
    private func setupInitialState() {
        backgroundColor = AYMColor.clear
        conditionValueLabel.textColor = AYMColor.brown
        conditionUnitLabel.textColor = AYMColor.brown
        conditionNameLabel.textColor = AYMColor.lightBrown
    }

    func setup(with viewModel: ViewModel) {
        conditionValueLabel.text = viewModel.conditionValue
        conditionUnitLabel.text = viewModel.conditionUnit
        conditionNameLabel.text = viewModel.conditionName
    }
}
