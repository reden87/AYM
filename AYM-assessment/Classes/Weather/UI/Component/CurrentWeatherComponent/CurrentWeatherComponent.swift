//
//  CurrentWeatherComponent.swift
//  AYM-assessment
//
//  Created by Gergo Nemeth on 2018. 08. 11..
//  Copyright © 2018. Gergo Nemeth. All rights reserved.
//

import UIKit
import Kingfisher

final class CurrentWeatherComponent: AYMComponent {
    // MARK: - Constants
    private enum Constants {
        static let dashCharacter = "-"
        static let celsiusCharacter = "℃"
        static let imageLinkPattern = "https://openweathermap.org/img/w/%@.png"
    }

    // MARK: - ViewModel
    struct ViewModel {
        let locationName: String
        let weatherDescription: String
        let weatherIconName: String
        let temperature: Double
        let weatherConditionList: [WeatherConditionItem.ViewModel]
    }

    // MARK: - Outlets
    @IBOutlet private weak var locationNameLabel: UILabel!
    @IBOutlet private weak var dayOfWeekLabel: UILabel!
    @IBOutlet private weak var weatherDescriptionLabel: UILabel!
    @IBOutlet private weak var weatherIconImageView: UIImageView!
    @IBOutlet private weak var temperatureLabel: UILabel!
    @IBOutlet private weak var celsiusLabel: UILabel!
    @IBOutlet private weak var weatherConditionsStackView: UIStackView!

    // MARK: - Lifecycle
    override func awakeFromNib() {
        super.awakeFromNib()
        setupInitialState()
    }
}

// MARK: - Setup
extension CurrentWeatherComponent {
    private func setupInitialState() {
        backgroundColor = AYMColor.lightOrange

        locationNameLabel.text = Constants.dashCharacter
        locationNameLabel.textColor = AYMColor.brown

        dayOfWeekLabel.text = Date().asString(format: .longNameForDay)
        dayOfWeekLabel.textColor = AYMColor.lightBrown

        weatherDescriptionLabel.text = ""
        weatherDescriptionLabel.textColor = AYMColor.lightBrown

        weatherIconImageView.image = nil

        temperatureLabel.text = Constants.dashCharacter
        temperatureLabel.textColor = AYMColor.brown

        celsiusLabel.text = Constants.celsiusCharacter
        celsiusLabel.textColor = AYMColor.brown
    }

    func setup(with viewModel: ViewModel) {
        locationNameLabel.text = viewModel.locationName
        weatherDescriptionLabel.text = viewModel.weatherDescription

        let imageUrl = URL(string: String(format: Constants.imageLinkPattern, viewModel.weatherIconName))
        weatherIconImageView.kf.setImage(with: imageUrl)

        temperatureLabel.text = String(describing: Int(viewModel.temperature))

        weatherConditionsStackView.removeAllArrangedSubviews()
        viewModel.weatherConditionList.forEach { conditionItemViewModel in
            let conditionItem = WeatherConditionItem.instantiateFromNib()
            conditionItem.setup(with: conditionItemViewModel)
            weatherConditionsStackView.addArrangedSubview(conditionItem)
        }
    }
}
