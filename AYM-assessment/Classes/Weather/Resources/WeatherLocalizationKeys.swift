//
//  WeatherLocalizationKeys.swift
//  AYM-assessment
//
//  Created by Gergo Nemeth on 2018. 08. 11..
//  Copyright © 2018. Gergo Nemeth. All rights reserved.
//

enum WeatherKey {
    static let tabBarTitle = "weather_tab_bar_title"
    static let humidity = "weather_humidity_name"
    static let windSpeed = "weather_wind_speed_name"
    static let kilometerPerHour = "weather_kilometer_per_hour_unit_name"
}
