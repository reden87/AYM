//
//  WeatherDataManager.swift
//  AYM-assessment
//
//  Created by Gergo Nemeth on 2018. 08. 11..
//  Copyright © 2018. Gergo Nemeth. All rights reserved.
//

import Foundation

final class WeatherDataManager {
    // MARK: - Constants
    private enum Constants {
        static let percentageCharacter = "%"
    }
}

// MARK: - Current Weather
extension WeatherDataManager {
    func getCurrentWeather(latitude: Double,
                           longitude: Double,
                           callback: @escaping (Result<CurrentWeatherComponent.ViewModel>) -> Void) {
        WeatherProvider().getCurrentWeather(latitude: latitude, longitude: longitude) { [weak self] networkResult in
            switch networkResult {
            case .success(let viewModel):
                if let viewModel = self?.mapCurrentWeather(viewModel: viewModel) {
                    callback(.success(viewModel))
                }
                // no need for else here because if self is nil then it means our screen is not in memory any more..

            case .error(let error):
                callback(.error(error))
            }
        }
    }

    private func mapCurrentWeather(viewModel: CurrentWeatherModel) -> CurrentWeatherComponent.ViewModel {
        return CurrentWeatherComponent.ViewModel(
            locationName: viewModel.name,
            weatherDescription: viewModel.weatherDescription ?? "",
            weatherIconName: viewModel.weatherIconName ?? "",
            temperature: viewModel.temperature,
            weatherConditionList: [
                WeatherConditionItem.ViewModel(conditionName: WeatherKey.humidity.translate(),
                                               conditionValue: String(describing: Int(viewModel.humidity)),
                                               conditionUnit: Constants.percentageCharacter),
                WeatherConditionItem.ViewModel(conditionName: WeatherKey.windSpeed.translate(),
                                               conditionValue: String(describing: Int(viewModel.windSpeed)),
                                               conditionUnit: WeatherKey.kilometerPerHour.translate())
            ]
        )
    }
}

// MARK: - Forecast
extension WeatherDataManager {
    func getForecast(latitude: Double,
                     longitude: Double,
                     callback: @escaping (Result<WeatherForecastComponent.ViewModel>) -> Void) {
        WeatherProvider().getForecast(latitude: latitude, longitude: longitude) { [weak self] networkResult in
            switch networkResult {
            case .success(let viewModel):
                if let viewModel = self?.mapForecast(viewModel: viewModel) {
                    callback(.success(viewModel))
                }
                // no need for else here because if self is nil then it means our screen is not in memory any more..

            case .error(let error):
                callback(.error(error))
            }
        }
    }

    private func mapForecast(viewModel: WeatherForecastListModel) -> WeatherForecastComponent.ViewModel {
        var forecastItemViewModels: [WeatherForecastItem.ViewModel] = []

        var days: [String] = []
        var daysCount = 0
        viewModel.list
            .filter { weather in
                let date = weather.date
                if date < Date() {
                    return false
                }

                let shortNameForDay = date.asString(format: .shortNameForDay)
                if days.contains(shortNameForDay) || daysCount >= 5 {
                    return false
                }
                days.append(shortNameForDay)
                daysCount += 1
                return true
            }
            .forEach { weather in
                forecastItemViewModels.append(WeatherForecastItem.ViewModel(
                    date: weather.date,
                    weatherIconName: (weather.weather.first?.icon)!,
                    maxTemperature: weather.main.maxTemperature,
                    minTemperature: weather.main.minTemperature))
        }

        return WeatherForecastComponent.ViewModel(forecastItems: forecastItemViewModels)
    }
}
