//
//  RestaurantLocalizationKeys.swift
//  AYM-assessment
//
//  Created by Gergo Nemeth on 2018. 08. 11..
//  Copyright © 2018. Gergo Nemeth. All rights reserved.
//

enum RestaurantKeys {
    static let tabBarTitle = "restaurants_tab_bar_title"
    static let title = "restaurants_title"
    static let shortBy = "restaurants_short_by_button"
    static let filter = "restaurants_filter_button"
}
