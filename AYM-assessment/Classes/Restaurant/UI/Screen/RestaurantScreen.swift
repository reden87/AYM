//
//  RestaurantScreen.swift
//  AYM-assessment
//
//  Created by Gergo Nemeth on 2018. 08. 11..
//  Copyright © 2018. Gergo Nemeth. All rights reserved.
//

import UIKit
import Foundation

final class RestaurantScreen: StaticBaseScreenController {
    // MARK: - Properties
    private let dataManager = RestaurantDataManager()

    // MARK: - Components
    private let sortAndFilterComponent = SortAndFilterComponent.instantiateFromNib()
    private let restaurantListComponent = RestaurantListComponent.instantiateFromNib()

    // MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        title = RestaurantKeys.title.translate()
        setupComponents()
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        setupNavigationBar()
        startListeningLocation()
    }
}

// MARK: - Setup
extension RestaurantScreen {
    private func setupNavigationBar() {
        UIApplication.shared.statusBarStyle = .lightContent

        navigationController?.navigationBar.barTintColor = AYMColor.blue

        let textAttributes = [NSAttributedStringKey.foregroundColor: AYMColor.white]
        navigationController?.navigationBar.titleTextAttributes = textAttributes
    }

    private func setupComponents() {
        // temporary solution.. should find out why the content appears below the navigation bar
        if let navigationBarHeight = navigationController?.navigationBar.bounds.height {
            let statusBarHeight = UIApplication.shared.statusBarFrame.height
            addEmptyComponent(with: (navigationBarHeight + statusBarHeight))
        }

        setupSortAndFilterComponent()
        addComponent(restaurantListComponent)
    }

    private func setupSortAndFilterComponent() {
        sortAndFilterComponent.setup(with: SortAndFilterComponent.ViewModel(
            shortByTapped: {
                // TODO: implement
                print("shortByTapped")
            },
            filterTapped: {
                // TODO: implement
                print("filterTapped")
            }))
        addComponent(sortAndFilterComponent)
    }

    private func startListeningLocation() {
        LocationManager.shared.getLocation { [weak self] response in
            switch response {
            case .location(let coordinates):
                self?.loadDataFor(latitude: coordinates.latitude, longitude: coordinates.longitude)

            case .error(let error):
                // TODO: display error in component
                print("error occured: \(error)")
            }
        }
    }

    private func setupNearbyRestaurants(with viewModel: RestaurantListComponent.ViewModel) {
        restaurantListComponent.setup(with: viewModel)
    }
}

// MARK: - Load Data
extension RestaurantScreen {
    private func loadDataFor(latitude: Double, longitude: Double) {
        dataManager.getNearbyRestaurants(latitude: latitude, longitude: longitude) { [weak self] result in
            switch result {
            case .success(let viewModel):
                self?.setupNearbyRestaurants(with: viewModel)

            case .error(let error):
                // TODO: display error in component
                print("error occured: \(error)")
            }
        }
    }
}
