//
//  SortAndFilterComponent.swift
//  AYM-assessment
//
//  Created by Gergo Nemeth on 2018. 08. 12..
//  Copyright © 2018. Gergo Nemeth. All rights reserved.
//

import UIKit

final class SortAndFilterComponent: AYMComponent {
    // MARK: - Constants
    private enum Constants {
        static let buttonNormalColor = AYMColor.black
    }

    // MARK: - ViewModel
    struct ViewModel {
        let shortByTapped: EmptyCallback
        let filterTapped: EmptyCallback
    }

    // MARK: - Outlets
    @IBOutlet private weak var shortByButton: UIButton!
    @IBOutlet private weak var separatorWidthConstraint: NSLayoutConstraint!
    @IBOutlet private weak var filterButton: UIButton!
    @IBOutlet private weak var bottomSeparatorHeightConstraint: NSLayoutConstraint!

    // MARK: - Properties
    private var viewModel: ViewModel?

    // MARK: - Lifecycle
    override func awakeFromNib() {
        super.awakeFromNib()
        setupInitialState()
    }
}

// MARK: - Setup
extension SortAndFilterComponent {
    private func setupInitialState() {
        shortByButton.setTitleColor(AYMColor.black, for: .normal)
        shortByButton.setTitleColor(AYMColor.blue, for: .selected)
        shortByButton.setTitle(RestaurantKeys.shortBy.translate().uppercased(), for: .normal)

        separatorWidthConstraint.constant = 1.0 / UIScreen.main.scale

        filterButton.setTitleColor(AYMColor.black, for: .normal)
        filterButton.setTitleColor(AYMColor.blue, for: .selected)
        filterButton.setTitle(RestaurantKeys.filter.translate().uppercased(), for: .normal)

        bottomSeparatorHeightConstraint.constant = 1.0 / UIScreen.main.scale
    }

    func setup(with viewModel: ViewModel) {
        self.viewModel = viewModel
    }
}

// MARK: - Actions
extension SortAndFilterComponent {
    @IBAction private func shortByButtonTapped() {
        viewModel?.shortByTapped()
    }

    @IBAction private func filterButtonTapped() {
        viewModel?.filterTapped()
    }
}
