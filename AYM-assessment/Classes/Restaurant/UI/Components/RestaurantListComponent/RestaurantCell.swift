//
//  RestaurantCell.swift
//  AYM-assessment
//
//  Created by Gergo Nemeth on 2018. 08. 12..
//  Copyright © 2018. Gergo Nemeth. All rights reserved.
//

import UIKit
import Kingfisher

final class RestaurantCell: UITableViewCell {
    // MARK: - Constants
    private enum Constants {
        static let imageDimensions = 100 * Int(UIScreen.main.scale)
        static let imageLinkPattern = "https://maps.googleapis.com/maps/api/place/photo"
            + "?maxwidth=\(imageDimensions)"
            + "&maxheight=\(imageDimensions)"
            + "&photoreference=%@"
            + "&key=\(RestaurantService.Constants.googlePlacesApiKey)"
        static let navigationImageName = "NavigationIcon"
    }

    // MARK: - ViewModel
    struct ViewModel {
        let imageReference: String?
        let restaurantName: String
        let rating: Double
        let latitude: Double?
        let longitude: Double?
    }

    // MARK: - Outlets
    @IBOutlet private weak var previewImageView: UIImageView!
    @IBOutlet private weak var restaurantNameLabel: UILabel!
    @IBOutlet private weak var navigationButton: UIButton!
    @IBOutlet private weak var ratingLabel: UILabel!

    // MARK: - Properties
    private var viewModel: ViewModel?

    // MARK: - Lifecycle
    override func awakeFromNib() {
        super.awakeFromNib()
        setupInitialState()
    }

    override func prepareForReuse() {
        super.prepareForReuse()
        previewImageView.image = nil
        restaurantNameLabel.text = ""
        ratingLabel.text = ""
    }
}

// MARK: - Setup
extension RestaurantCell {
    private func setupInitialState() {
        previewImageView.backgroundColor = AYMColor.lightGrey
        restaurantNameLabel.textColor = AYMColor.black
        ratingLabel.textColor = AYMColor.black

        navigationButton.setImage(
            UIImage(imageLiteralResourceName: Constants.navigationImageName).withRenderingMode(.alwaysTemplate),
            for: .normal)
        navigationButton.tintColor = AYMColor.blue
    }

    func setup(with viewModel: ViewModel) {
        self.viewModel = viewModel

        if let imageReference = viewModel.imageReference {
            let imageUrl = URL(string: String(format: Constants.imageLinkPattern, imageReference))
            previewImageView.kf.setImage(with: imageUrl)
        }

        restaurantNameLabel.text = viewModel.restaurantName
        ratingLabel.text = String(describing: viewModel.rating) + " ⭐️"
    }
}

// MARK: - Actions
extension RestaurantCell {
    @IBAction private func navigationButtonTapped() {
        if let latitude = viewModel?.latitude, let longitude = viewModel?.longitude {
            // TODO
            print("navigate to (lat: \(latitude), long: \(longitude))")
        }
    }
}
