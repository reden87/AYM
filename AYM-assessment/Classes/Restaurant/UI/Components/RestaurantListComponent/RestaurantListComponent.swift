//
//  RestaurantListComponent.swift
//  AYM-assessment
//
//  Created by Gergo Nemeth on 2018. 08. 11..
//  Copyright © 2018. Gergo Nemeth. All rights reserved.
//

import UIKit

final class RestaurantListComponent: AYMComponent {
    // MARK: - Constants
    private enum Constants {
        static let cellIdentifier = String(describing: RestaurantCell.self)
    }

    // MARK: - ViewModel
    struct ViewModel {
        let restaurantList: [RestaurantCell.ViewModel]
    }

    // MARK: - Outlets
    @IBOutlet private weak var tableView: UITableView!

    // MARK: - Properties
    private var viewModel: ViewModel?

    // MARK: - Lifecycle
    override func awakeFromNib() {
        super.awakeFromNib()
        setupInitialState()
    }
}

// MARK: - Setup
extension RestaurantListComponent {
    private func setupInitialState() {
        tableView.dataSource = self
        tableView.delegate = self

        let cellNib = UINib(nibName: Constants.cellIdentifier, bundle: nil)
        tableView.register(cellNib, forCellReuseIdentifier: Constants.cellIdentifier)

        tableView.tableFooterView = UIView(frame: .zero)
    }

    func setup(with viewModel: ViewModel) {
        self.viewModel = viewModel
        tableView.reloadData()
    }
}

// MARK: - UITableViewDataSource
extension RestaurantListComponent: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel?.restaurantList.count ?? 0
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: Constants.cellIdentifier)
            ?? UITableViewCell(style: .default, reuseIdentifier: Constants.cellIdentifier)

        if let cell = cell as? RestaurantCell, let viewModel = viewModel?.restaurantList[indexPath.row] {
            cell.setup(with: viewModel)
        }

        return cell
    }
}

// MARK: - UITableViewDelegate
extension RestaurantListComponent: UITableViewDelegate {
}
