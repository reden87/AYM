//
//  RestaurantDataManager.swift
//  AYM-assessment
//
//  Created by Gergo Nemeth on 2018. 08. 11..
//  Copyright © 2018. Gergo Nemeth. All rights reserved.
//

final class RestaurantDataManager {
}

// MARK: - Nearby Restaurants
extension RestaurantDataManager {
}

// MARK: - Current Weather
extension RestaurantDataManager {
    func getNearbyRestaurants(latitude lat: Double,
                              longitude lon: Double,
                              callback: @escaping (Result<RestaurantListComponent.ViewModel>) -> Void) {
        RestaurantProvider().getNearbyRestaurants(latitude: lat, longitude: lon) { [weak self] networkResult in
            switch networkResult {
            case .success(let viewModel):
                if let viewModel = self?.mapNearbyRestaurants(viewModel: viewModel) {
                    callback(.success(viewModel))
                }
                // no need for else here because if self is nil then it means our screen is not in memory any more..

            case .error(let error):
                callback(.error(error))
            }
        }
    }

    private func mapNearbyRestaurants(viewModel: NearbyRestaurantsModel) -> RestaurantListComponent.ViewModel {
        var restaurantList: [RestaurantCell.ViewModel] = []

        viewModel.results.forEach { restaurant in
            let model = RestaurantCell.ViewModel(imageReference: restaurant.photos?.first?.photoReference,
                                                 restaurantName: restaurant.name,
                                                 rating: restaurant.rating,
                                                 latitude: restaurant.geometry?.location?.latitude,
                                                 longitude: restaurant.geometry?.location?.longitude)
            restaurantList.append(model)
        }

        return RestaurantListComponent.ViewModel(restaurantList: restaurantList)
    }
}
