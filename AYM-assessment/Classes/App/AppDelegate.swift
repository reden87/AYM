//
//  AppDelegate.swift
//  AYM-assessment
//
//  Created by Gergo Nemeth on 2018. 08. 07..
//  Copyright © 2018. Gergo Nemeth. All rights reserved.
//

import UIKit
import Fabric
import Crashlytics

@UIApplicationMain
class AppDelegate: UIResponder {
    // MARK: - Constants
    private enum Constants {
        static let weatherScreenTabBarIcon = UIImage(named: "TabBarWeatherIcon")
        static let weatherScreenTabBarIconFilled = UIImage(named: "TabBarWeatherIconFilled")
        static let restaurantsScreenTabBarIcon = UIImage(named: "TabBarRestaurantIcon")
        static let restaurantsScreenTabBarIconFilled = UIImage(named: "TabBarRestaurantIconFilled")
    }

    // MARK: - Properties
    var window: UIWindow?
}

// MARK: - UIApplicationDelegate
extension AppDelegate: UIApplicationDelegate {
    func application(_ application: UIApplication,
                     didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        // crash reporting
        Fabric.with([Crashlytics.self])

        // display initial state of the app
        setupWindow()

        return true
    }
}

// MARK: - Setup
extension AppDelegate {
    private func setupWindow() {
        let tabBarController = UITabBarController()
        tabBarController.tabBar.barTintColor = AYMColor.blue
        tabBarController.tabBar.tintColor = AYMColor.white
        tabBarController.tabBar.unselectedItemTintColor = AYMColor.brown
        tabBarController.tabBar.isOpaque = false

        let weatherScreen = WeatherScreen()
        weatherScreen.tabBarItem = UITabBarItem(title: WeatherKey.tabBarTitle.translate(),
                                                image: Constants.weatherScreenTabBarIcon,
                                                selectedImage: Constants.weatherScreenTabBarIconFilled)

        let restaurantsScreen = RestaurantScreen()
        let restaurantsNavigationController = UINavigationController(rootViewController: restaurantsScreen)
        restaurantsNavigationController.tabBarItem =
            UITabBarItem(title: RestaurantKeys.tabBarTitle.translate(),
                         image: Constants.restaurantsScreenTabBarIcon,
                         selectedImage: Constants.restaurantsScreenTabBarIconFilled)

        tabBarController.viewControllers = [weatherScreen, restaurantsNavigationController]

        window = UIWindow(frame: UIScreen.main.bounds)
        window?.rootViewController = tabBarController
        window?.makeKeyAndVisible()
    }
}
