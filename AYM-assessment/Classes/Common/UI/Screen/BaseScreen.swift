//
//  BaseScreen.swift
//  AYM-assessment
//
//  Created by Gergo Nemeth on 2018. 08. 11..
//  Copyright © 2018. Gergo Nemeth. All rights reserved.
//

import UIKit

class BaseScreen: UIViewController {
    // MARK: - Private
    private let stackView: UIStackView = UIStackView(frame: .zero)

    // MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()

        view.backgroundColor = AYMColor.white
        automaticallyAdjustsScrollViewInsets = false
        setupStackView()
    }
}

// MARK: - StackView
extension BaseScreen {
    private func setupStackView() {
        stackView.axis = .vertical
        stackView.clipsToBounds = false
        stackView.translatesAutoresizingMaskIntoConstraints = false
    }

    func setStackViewSpacing(with spacing: CGFloat) {
        stackView.spacing = spacing
    }

    func attachStackView(to view: UIView, topPadding: CGFloat = 0.0) {
        view.addSubview(stackView)

        stackView.anchorToSuperview()
        stackView.widthAnchor.constraint(equalTo: view.widthAnchor).isActive = true
    }
}

// MARK: - Manage Components
extension BaseScreen {
    func addComponent(_ component: AYMComponent) {
        stackView.addArrangedSubview(component)
    }

    func addEmptyComponent(with height: CGFloat) {
        let emptyComponent = EmptyComponent.instantiateFromNib()
        emptyComponent.setup(with: height)
        addComponent(emptyComponent)
    }

    func removeComponent(_ component: AYMComponent) {
        stackView.removeArrangedSubview(component)
    }

    func removeAllComponents() {
        stackView.removeAllArrangedSubviews()
    }
}
