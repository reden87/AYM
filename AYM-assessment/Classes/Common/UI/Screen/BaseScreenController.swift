//
//  BaseScreenController.swift
//  AYM-assessment
//
//  Created by Gergo Nemeth on 2018. 08. 11..
//  Copyright © 2018. Gergo Nemeth. All rights reserved.
//

import UIKit

class BaseScreenController: BaseScreen {
    // MARK: - Properties
    internal lazy var scrollView = createScrollView()

    // MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        attachStackView(to: scrollView)
    }
}

// MARK: - Lazy
extension BaseScreenController {
    private func createScrollView() -> UIScrollView {
        let scrollView = UIScrollView(frame: .zero)
        scrollView.delaysContentTouches = false
        //scrollView.delegate = self

        scrollView.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(scrollView)
        scrollView.anchorToSuperview()

        return scrollView
    }
}
