//
//  StaticBaseScreenController.swift
//  AYM-assessment
//
//  Created by Gergo Nemeth on 2018. 08. 11..
//  Copyright © 2018. Gergo Nemeth. All rights reserved.
//

import UIKit

class StaticBaseScreenController: BaseScreen {
    // MARK: - Properties
    private lazy var containerView = createContainerView()

    // MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        attachStackView(to: containerView)
    }
}

// MARK: - Lazy
extension StaticBaseScreenController {
    private func createContainerView() -> UIView {
        let containerView = UIView(frame: .zero)

        containerView.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(containerView)
        containerView.anchorToSuperview()

        return containerView
    }
}
