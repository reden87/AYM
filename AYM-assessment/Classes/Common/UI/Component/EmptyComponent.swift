//
//  EmptyComponent.swift
//  AYM-assessment
//
//  Created by Gergo Nemeth on 2018. 08. 12..
//  Copyright © 2018. Gergo Nemeth. All rights reserved.
//

import UIKit

final class EmptyComponent: AYMComponent {
    // MARK: - Properties
    @IBOutlet private weak var heightConstraint: NSLayoutConstraint!
}

// MARK: - Setup
extension EmptyComponent {
    func setup(with height: CGFloat) {
        heightConstraint.constant = height
    }
}
