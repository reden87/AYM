//
//  AYMColor.swift
//  AYM-assessment
//
//  Created by Gergo Nemeth on 2018. 08. 11..
//  Copyright © 2018. Gergo Nemeth. All rights reserved.
//

import UIKit

struct AYMColor {
    static var clear: UIColor { return UIColor.clear }
    static var white: UIColor { return UIColor.white }
    static var black: UIColor { return UIColor.black }
    static var blue: UIColor { return UIColor(hex: 0x4e92df) }
    static var lightGrey: UIColor { return UIColor.lightGray }
    static var brown: UIColor { return UIColor(hex: 0x5a4226) }
    static var lightBrown: UIColor { return UIColor(hex: 0x8b663d) }
    static var lightOrange: UIColor { return UIColor(hex: 0xfbd9b2) }
}
