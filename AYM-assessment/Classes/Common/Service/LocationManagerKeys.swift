//
//  LocationManagerKeys.swift
//  AYM-assessment
//
//  Created by Gergo Nemeth on 2018. 08. 08..
//  Copyright © 2018. Gergo Nemeth. All rights reserved.
//

enum LocationManagerKeys {
    enum Error {
        static let title = "locationmanager_error_title"
        static let message = "locationmanager_error_message"
        static let settingsButton = "locationmanager_error_settings_button"
        static let cancelButton = "locationmanager_error_cancel_button"
    }
}
