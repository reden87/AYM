//
//  LocationManager.swift
//  AYM-assessment
//
//  Created by Gergo Nemeth on 2018. 08. 08..
//  Copyright © 2018. Gergo Nemeth. All rights reserved.
//

import CoreLocation
import UIKit

enum LocationManagerError: Error {
    case locationNotAvailableError
    case locationPermissionDeniedError
    case otherError(Error)
}

enum LocationManagerResponse {
    case location(CLLocationCoordinate2D)
    case error(LocationManagerError)
}

typealias LocationResponse = (_ response: LocationManagerResponse) -> Void

final class LocationManager: NSObject {
    private lazy var locationManager = setupLocationManager()
    private var callback: LocationResponse?

    // MARK: - SharedInstance
    static let shared = LocationManager()
    private override init() {} //This prevents others from using the default '()' initializer for this class.
}

// MARK: - Setup
extension LocationManager {
    private func setupLocationManager() -> CLLocationManager {
        let locationManager = CLLocationManager()
        locationManager.delegate = self
        return locationManager
    }
}

// MARK: - Public
extension LocationManager {
    func getLocation(_ callback: @escaping LocationResponse) {
        self.callback = callback
        getLocation()
    }
}

// MARK: - Private
extension LocationManager {
    private func getLocation() {
        switch CLLocationManager.authorizationStatus() {
        case .notDetermined:
            askPermission()
        case .restricted:
            callback?(.error(.locationNotAvailableError))
        case .denied:
            showNoPermissionPopup()
        case .authorizedWhenInUse:
            locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
            locationManager.startUpdatingLocation()
        default: break
        }
    }

    private func askPermission() {
        locationManager.requestWhenInUseAuthorization()
    }

    private func showNoPermissionPopup() {
        let alertController = UIAlertController(title: LocationManagerKeys.Error.title.translate(),
                                                message: LocationManagerKeys.Error.message.translate(),
                                                preferredStyle: .alert)

        alertController.addAction(UIAlertAction(
            title: LocationManagerKeys.Error.settingsButton.translate(),
            style: .cancel,
            handler: { [weak self] _ in
                UIApplication.shared.open(URL(string: UIApplicationOpenSettingsURLString)!)
                self?.callback?(.error(.locationPermissionDeniedError))
        }))
        alertController.addAction(UIAlertAction(
            title: LocationManagerKeys.Error.cancelButton.translate(),
            style: .destructive,
            handler: { [weak self] _ in
                self?.callback?(.error(.locationPermissionDeniedError))
        }))

        UIApplication.shared.keyWindow?.rootViewController?.present(alertController, animated: true)
    }
}

// MARK: - CLLocationManagerDelegate
extension LocationManager: CLLocationManagerDelegate {
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        getLocation()
    }

    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        callback?(.error(.otherError(error)))
        locationManager.stopUpdatingLocation()
    }

    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if let location = locations.first {
            callback?(.location(location.coordinate))
            locationManager.stopUpdatingLocation()
        }
    }
}
