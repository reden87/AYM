//
//  ResultType.swift
//  AYM-assessment
//
//  Created by Gergo Nemeth on 2018. 08. 12..
//  Copyright © 2018. Gergo Nemeth. All rights reserved.
//

enum Result<Value> {
    case success(Value)
    case error(Error)
}
