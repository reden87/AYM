//
//  CurrentWeatherModel.swift
//  AYM-assessment
//
//  Created by Gergo Nemeth on 2018. 08. 10..
//  Copyright © 2018. Gergo Nemeth. All rights reserved.
//

struct CurrentWeatherModel: Decodable {
    let name: String
    let weatherDescription: String?
    let weatherIconName: String?
    let temperature: Double
    let humidity: Double
    let windSpeed: Double

    // MARK: - Mapping nested keys
    init(from decoder: Decoder) throws {
        let rawResponse = try RawCurrentWeatherResponse(from: decoder)

        name = rawResponse.name
        weatherDescription = rawResponse.weather.first?.description
        weatherIconName = rawResponse.weather.first?.icon
        temperature = rawResponse.main.temp
        humidity = rawResponse.main.humidity
        windSpeed = rawResponse.wind.speed
    }
}

// MARK: - Decoding raw response
private struct RawCurrentWeatherResponse: Decodable {
    let name: String
    let weather: [Weather]
    let main: Main
    let wind: Wind

    struct Weather: Decodable {
        let description: String
        let icon: String
    }

    struct Main: Decodable {
        let temp: Double
        let humidity: Double
    }

    struct Wind: Decodable {
        let speed: Double
    }
}
