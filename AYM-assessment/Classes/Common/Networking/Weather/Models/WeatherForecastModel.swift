//
//  WeatherForecastModel.swift
//  AYM-assessment
//
//  Created by Gergo Nemeth on 2018. 08. 10..
//  Copyright © 2018. Gergo Nemeth. All rights reserved.
//

import Foundation

// MARK: - Decoding raw response
// swiftlint:disable nesting
struct WeatherForecastListModel: Decodable {
    let list: [Day]

    struct Day: Decodable {
        let date: Date
        let main: Main
        let weather: [Weather]

        enum CodingKeys: String, CodingKey {
            case main, weather
            case date = "dt"
        }
    }

    struct Main: Decodable {
        let minTemperature: Double
        let maxTemperature: Double

        enum CodingKeys: String, CodingKey {
            case minTemperature = "temp_min"
            case maxTemperature = "temp_max"
        }
    }

    struct Weather: Decodable {
        let icon: String
    }
}
// swiftlint:enable nesting
