//
//  WeatherService.swift
//  AYM-assessment
//
//  Created by Gergo Nemeth on 2018. 08. 10..
//  Copyright © 2018. Gergo Nemeth. All rights reserved.
//

import Moya

enum WeatherService {
    // MARK: - Constants
    private enum Constants {
        static let baseUrl = "https://api.openweathermap.org/data/2.5/"

        static let currentWeatherEndpoint = "weather"
        static let weatherForecastEndpoint = "forecast"

        static let openWeatherMapApiKey = "395a99eff10ea5f87958bc32bf16e4d9"
        static let defaultGetParamerets = [
            "units": "metric",
            "appid": Constants.openWeatherMapApiKey
        ]
        static let latitudeParameterKey = "lat"
        static let longitudeParameterKey = "lon"

        static let currentWeatherSampleFileName = "CurrentWeatherSample"
        static let weatherForecastSampleFileName = "ForecastSample"
        static let samplesFileExtension = "json"
    }

    // MARK: - Cases
    case getCurrent(lat: Double, long: Double)
    case getForecast(lat: Double, long: Double)
}

// MARK: - TargetType
extension WeatherService: TargetType {
    var baseURL: URL { return URL(string: Constants.baseUrl)! }

    var path: String {
        switch self {
        case .getCurrent: return Constants.currentWeatherEndpoint
        case .getForecast: return Constants.weatherForecastEndpoint
        }
    }

    var method: Method { return .get }

    var sampleData: Data {
        switch self {
        case .getCurrent: return sampleDataForCurrent()
        case .getForecast: return sampleDataForForecast()
        }
    }

    var task: Task {
        var defaultGetParamerets = Constants.defaultGetParamerets

        switch self {
        case .getCurrent(let lat, let long):
            defaultGetParamerets[Constants.latitudeParameterKey] = String(describing: lat)
            defaultGetParamerets[Constants.longitudeParameterKey] = String(describing: long)
            return .requestCompositeData(bodyData: Data(), urlParameters: defaultGetParamerets)

        case .getForecast(let lat, let long):
            defaultGetParamerets[Constants.latitudeParameterKey] = String(describing: lat)
            defaultGetParamerets[Constants.longitudeParameterKey] = String(describing: long)
            return .requestCompositeData(bodyData: Data(), urlParameters: defaultGetParamerets)
        }
    }

    var headers: [String: String]? { return nil }
}

// MARK: - Helpers (sample data)
private extension WeatherService {
    func sampleDataForCurrent() -> Data {
        let sampleString =
            String.readFile(name: Constants.currentWeatherSampleFileName, type: Constants.samplesFileExtension)
        return sampleString?.data(using: .utf8) ?? Data()
    }

    private func sampleDataForForecast() -> Data {
        let sampleString =
            String.readFile(name: Constants.weatherForecastSampleFileName, type: Constants.samplesFileExtension)
        return sampleString?.data(using: .utf8) ?? Data()
    }
}
