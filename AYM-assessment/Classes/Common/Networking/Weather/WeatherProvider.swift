//
//  WeatherProvider.swift
//  AYM-assessment
//
//  Created by Gergo Nemeth on 2018. 08. 10..
//  Copyright © 2018. Gergo Nemeth. All rights reserved.
//

import Moya

final class WeatherProvider: ProviderBase {
    // MARK: - Constants
    private enum Constants {
        static let providerPlugins: [PluginType] = [] // [NetworkLoggerPlugin(verbose: true)]
    }

    // MARK: - Private
    private let provider = MoyaProvider<WeatherService>(plugins: Constants.providerPlugins)
}

// MARK: - Public
extension WeatherProvider {
    func getCurrentWeather(latitude: Double,
                           longitude: Double,
                           callback: @escaping (Result<CurrentWeatherModel>) -> Void) {
        sendMoyaRequest(with: .getCurrent(lat: latitude, long: longitude), provider: provider, callback: callback)
    }

    func getForecast(latitude: Double,
                     longitude: Double,
                     callback: @escaping (Result<WeatherForecastListModel>) -> Void) {
        sendMoyaRequest(with: .getForecast(lat: latitude, long: longitude), provider: provider, callback: callback)
    }
}
