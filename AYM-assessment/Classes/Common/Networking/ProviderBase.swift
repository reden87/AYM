//
//  ProviderBase.swift
//  AYM-assessment
//
//  Created by Gergo Nemeth on 2018. 08. 12..
//  Copyright © 2018. Gergo Nemeth. All rights reserved.
//

import Moya

class ProviderBase {
}

// MARK: - Helper
extension ProviderBase {
    internal
    func sendMoyaRequest<Type: TargetType, ResultValue: Decodable>(with target: Type,
                                                                   provider: MoyaProvider<Type>,
                                                                   callback: @escaping (Result<ResultValue>) -> Void) {
        print("🤷‍♂️ request sent \(target)")
        provider.request(target) { result in
            switch result {
            case .success(let moyaResponse):
                print("✅ success \(target)")
                let result: Result<ResultValue> = self.map(moyaResponse: moyaResponse)
                return callback(result)

            case .failure(let error):
                print("🚨 request error \(target.self): \(error)")
                return callback(.error(error))
            }
        }
    }

    private func map<T: Decodable>(moyaResponse: Response) -> Result<T> {
        do {
            let filteredResponse = try moyaResponse.filterSuccessfulStatusCodes()
            let model = try filteredResponse.map(T.self)
            return .success(model)
        } catch let error {
            print("🚨 mapping error \(T.self): \(error)")
            return .error(error)
        }
    }
}
