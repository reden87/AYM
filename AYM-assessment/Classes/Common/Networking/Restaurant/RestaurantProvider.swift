//
//  RestaurantProvider.swift
//  AYM-assessment
//
//  Created by Gergo Nemeth on 2018. 08. 12..
//  Copyright © 2018. Gergo Nemeth. All rights reserved.
//

import Moya

final class RestaurantProvider: ProviderBase {
    // MARK: - Constants
    private enum Constants {
        static let providerPlugins: [PluginType] = [] // [NetworkLoggerPlugin(verbose: true)]
    }

    // MARK: - Private
    private let provider = MoyaProvider<RestaurantService>(plugins: Constants.providerPlugins)
}

// MARK: - Public
extension RestaurantProvider {
    func getNearbyRestaurants(latitude: Double,
                              longitude: Double,
                              callback: @escaping (Result<NearbyRestaurantsModel>) -> Void) {
        sendMoyaRequest(with: .getNearbyRestaurants(lat: latitude, long: longitude),
                        provider: provider,
                        callback: callback)
    }
}
