//
//  RestaurantService.swift
//  AYM-assessment
//
//  Created by Gergo Nemeth on 2018. 08. 12..
//  Copyright © 2018. Gergo Nemeth. All rights reserved.
//

import Moya

enum RestaurantService {
    // MARK: - Constants
    enum Constants {
        static let baseUrl = "https://maps.googleapis.com/maps/api/place/"

        static let restaurantsEndpoint = "nearbysearch/json"

        static let googlePlacesApiKey = "AIzaSyDSMSjnx8ShLyKE8qvxQC9dJfDbUpzbtp8"
        static let radiusParameter = "1500"
        static let defaultGetParamerets = [
            "type": "restaurant",
            "radius": Constants.radiusParameter,
            "key": Constants.googlePlacesApiKey
        ]
        static let locationParameterKey = "location"

        static let nearbyRestaurantsSampleFileName = "NearbyRestaurantsSample"
        static let samplesFileExtension = "json"
    }

    // MARK: - Cases
    case getNearbyRestaurants(lat: Double, long: Double)
}

// MARK: - TargetType
extension RestaurantService: TargetType {
    var baseURL: URL { return URL(string: Constants.baseUrl)! }
    var path: String { return Constants.restaurantsEndpoint }
    var method: Method { return .get }
    var headers: [String: String]? { return nil }
    var sampleData: Data { return sampleDataForNearbyRestaurants() }

    var task: Task {
        switch self {
        case .getNearbyRestaurants(let lat, let long):
            var defaultGetParamerets = Constants.defaultGetParamerets
            defaultGetParamerets[Constants.locationParameterKey] = "\(lat),\(long)"
            return .requestCompositeData(bodyData: Data(), urlParameters: defaultGetParamerets)
        }
    }
}

// MARK: - Helpers (sample data)
private extension RestaurantService {
    func sampleDataForNearbyRestaurants() -> Data {
        let sampleString =
            String.readFile(name: Constants.nearbyRestaurantsSampleFileName, type: Constants.samplesFileExtension)
        return sampleString?.data(using: .utf8) ?? Data()
    }
}
