//
//  NearbyRestaurantsModel.swift
//  AYM-assessment
//
//  Created by Gergo Nemeth on 2018. 08. 12..
//  Copyright © 2018. Gergo Nemeth. All rights reserved.
//

import Foundation

// swiftlint:disable nesting
struct NearbyRestaurantsModel: Decodable {
    let results: [Result]

    struct Result: Decodable {
        let name: String
        let icon: String
        let rating: Double = 4.7
        let photos: [Photo]?
        let geometry: Geometry?
    }

    struct Photo: Decodable {
        let photoReference: String

        enum CodingKeys: String, CodingKey {
            case photoReference = "photo_reference"
        }
    }

    struct Geometry: Decodable {
        let location: Location?
    }

    struct Location: Decodable {
        let latitude: Double
        let longitude: Double

        enum CodingKeys: String, CodingKey {
            case latitude = "lat"
            case longitude = "lng"
        }
    }
}
// swiftlint:enable nesting
