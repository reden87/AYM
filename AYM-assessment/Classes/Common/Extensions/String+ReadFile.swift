//
//  String+Read.swift
//  AYM-assessment
//
//  Created by Gergo Nemeth on 2018. 08. 10..
//  Copyright © 2018. Gergo Nemeth. All rights reserved.
//

import Foundation

extension String {
    static func readFile(name fileName: String, type fileType: String) -> String? {
        if let filePath = Bundle.main.path(forResource: fileName, ofType: fileType),
           let fileContent = try? String.init(contentsOfFile: filePath) {
            return fileContent
        }
        return nil
    }
}
