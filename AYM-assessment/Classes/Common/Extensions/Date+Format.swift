//
//  Date+Format.swift
//  AYM-assessment
//
//  Created by Gergo Nemeth on 2018. 08. 11..
//  Copyright © 2018. Gergo Nemeth. All rights reserved.
//

import Foundation

enum DateFormatterType: String {
    case shortNameForDay = "E"
    case longNameForDay = "EEEE"
}

extension Date {
    func asString(format: DateFormatterType) -> String {
        let dateFormatter = DateFormatter(with: format)
        return dateFormatter.string(from: self)
    }
}
