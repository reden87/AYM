//
//  String+Localize.swift
//  AYM-assessment
//
//  Created by Gergo Nemeth on 2018. 08. 08..
//  Copyright © 2018. Gergo Nemeth. All rights reserved.
//

import Foundation

extension String {
    func translate() -> String {
        return translate(with: [])
    }

    func translate(with args: CVarArg...) -> String {
        return NSLocalizedString(String(format: self, args), comment: "")
    }
}
