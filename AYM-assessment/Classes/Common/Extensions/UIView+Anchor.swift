//
//  UIView+Anchor.swift
//  AYM-assessment
//
//  Created by Gergo Nemeth on 2018. 08. 11..
//  Copyright © 2018. Gergo Nemeth. All rights reserved.
//

import UIKit

extension UIView {
    public func anchorToSuperview(top: Bool = true, bottom: Bool = true, leading: Bool = true, trailing: Bool = true) {
        guard let superview = self.superview else {
            return
        }
        self.topAnchor.constraint(equalTo: superview.topAnchor).isActive = top
        self.bottomAnchor.constraint(equalTo: superview.bottomAnchor).isActive = bottom
        self.leadingAnchor.constraint(equalTo: superview.leadingAnchor).isActive = leading
        self.trailingAnchor.constraint(equalTo: superview.trailingAnchor).isActive = trailing
    }
}
