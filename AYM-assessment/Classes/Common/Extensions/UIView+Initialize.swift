//
//  UIView+Initialize.swift
//  AYM-assessment
//
//  Created by Gergo Nemeth on 2018. 08. 11..
//  Copyright © 2018. Gergo Nemeth. All rights reserved.
//

import UIKit

extension UIView {
    public class func instantiateFromNib() -> Self {
        return loadFromNib(viewType: self)
    }

    //swiftlint:disable force_cast
    private class func loadFromNib<T: UIView>(viewType: T.Type) -> T {
        let url: NSURL! = NSURL(string: NSStringFromClass(viewType))
        return Bundle.main.loadNibNamed(url.pathExtension!, owner: nil, options: nil)?.first as! T
    }
    //swiftlint:enable force_cast
}
