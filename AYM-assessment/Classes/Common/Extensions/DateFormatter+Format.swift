//
//  DateFormatter+Format.swift
//  AYM-assessment
//
//  Created by Gergo Nemeth on 2018. 08. 11..
//  Copyright © 2018. Gergo Nemeth. All rights reserved.
//

import Foundation
import UIKit

extension DateFormatter {
    // MARK: - Constants
    private enum Constants {
        static let defaultLocale = "en"
    }

    // MARK: - Init
    convenience init(with type: DateFormatterType) {
        self.init()
        dateFormat = type.rawValue
        locale = Locale(identifier: Bundle.main.preferredLocalizations.first ?? Constants.defaultLocale)
    }
}
