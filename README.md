# AYM assessment

Created by Gergo Nemeth.

## Project features

### Project

* **.gitignore file**
	* reason: don't upload unnecessary (usually generated) files to GIT
	* usually I ignore the following files but I added them to GIT now so you can run the app instantly:
		* CocoaPods related files: **Pod folder**, *.xcworkspace file
		* cocoapods-acknowledgements related files: **\*/Settings.bundle/*-metadata.plist**

* **Gemfile and Gemfile.lock**
	* I use [Bundler](https://bundler.io) - I add the project dependenies to the Gemfile so every developer and the CI server also can use the same version of tools

* **CocoaPods**
	* reason: use popular 3rd party frameworks in the project, like
		* Fabric for beta distribution
		* Craslytics for crash reporting
		* SwiftLint for local syntax validation

* **2 separate scheme: development and production**
	* reason 1: 4 apps can be installed beside each other since they have different bundle identifier: development debug, development release, production debug, production release
	* reason 2: in Fabric all 4 build will have different project so the development and production crashes won't mix
	* reason 3: the development app has a "dev-" prefix in the app name so it'll be easier to differenciate

Usually I use **mock** scheme too which works with mocked data without internet connection. This is just for showcase purposes..

### UI

I built the UI with reusable independent components (UIView subclasses). Each component has a built-in ViewModel and can display it's ViewModel's data. If the component has to send events back to the screen it can do it via the callback closure inside the ViewModel.

I use 2 base ViewController subclasses:

* **BaseScreenController:** This is a UIViewController subclass. It contains a UIScrollView and inside it has a UIStackView. You can add infinite number of components to it. The screen will scroll if you add a lot of components. In this app I didn't use this class since the UI had different structure.

* **StaticBaseScreenController:** This is a UIViewController subclass too. It contains a UIView and inside it has a UIStackView. You can add finite number of components to it since this ViewController will stratch the components it comtains from the top of the screen to the bottom. The screen will not scroll if you add a lot of components. I used this class for both screens in this project.

## Screenshots

![MacDown Screenshot](Screenshots/Weather.jpg)
![MacDown Screenshot](Screenshots/Restaurants.jpg)

![MacDown Screenshot](Screenshots/Licenses-1.jpg)
![MacDown Screenshot](Screenshots/Licenses-2.jpg)
